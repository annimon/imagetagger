package com.annimon.imagetagger.views;

import java.awt.*;

public class UIUtils {

    public static void changeFontSize(Component component, float delta) {
        final var font = component.getFont();
        if (font == null) return;

        final float size = font.getSize2D() + delta;
        if (size <= 6) return;
        if (size >= 32) return;

        component.setFont(font.deriveFont(size));
        component.repaint();
    }
}
