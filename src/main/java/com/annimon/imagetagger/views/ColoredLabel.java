package com.annimon.imagetagger.views;

import java.awt.*;
import javax.swing.*;

public class ColoredLabel extends JLabel {

    private static final int MAX_COLORS = 8;
    private static final Color ENABLED_TEXT = Color.WHITE;
    private static final Color DISABLED_TEXT = new Color(0xA9A9A9);

    private final float hue;
    private boolean isToggled;

    public ColoredLabel(String key, String tag) {
        super(String.format("%s: %s", formatKeyAsHtml(key), tag));
        isToggled = false;
        hue = (tag.hashCode() % MAX_COLORS) * (1f / MAX_COLORS);
        init();
    }

    private void init() {
        setFont(getFont().deriveFont(11f));
        setBorder(BorderFactory.createEmptyBorder(3, 6, 3, 6));
        setOpaque(true);
        setToggled(false);
    }

    public void toggle() {
        setToggled(!isToggled);
    }

    public void setToggled(boolean toggled) {
        isToggled = toggled;
        if (toggled) {
            setBackground(Color.getHSBColor(hue, 0.90f, 0.65f));
            setForeground(ENABLED_TEXT);
        } else {
            setBackground(Color.getHSBColor(hue, 0.6f, 0.22f));
            setForeground(DISABLED_TEXT);
        }
    }

    public boolean isToggled() {
        return isToggled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
    }

    private static String formatKeyAsHtml(String key) {
        String result = "";
        String mutableKey = key;
        if (mutableKey.contains("Ctrl+")) {
            result += "Cᵀᴿᴸ+";
            mutableKey = mutableKey.replace("Ctrl+", "");
        }
        if (mutableKey.contains("Alt+")) {
            result += "Aᴸᵀ+";
            mutableKey = mutableKey.replace("Alt+", "");
        }
        result += mutableKey;
        return result;
    }
}
