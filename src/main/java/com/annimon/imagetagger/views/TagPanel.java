package com.annimon.imagetagger.views;

import com.annimon.imagetagger.beans.TagButton;
import com.annimon.imagetagger.beans.TagButtonHolder;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.swing.*;

public class TagPanel extends JPanel {

    private final Map<String, TagButtonHolder> tagButtons;

    public TagPanel(List<TagButton> buttons) {
        tagButtons = new LinkedHashMap<>();
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        addTags(buttons);
    }

    public void rebuildTags(List<TagButton> buttons) {
        removeAll();
        tagButtons.clear();
        addTags(buttons);
        validate();
    }

    private void addTags(List<TagButton> buttons) {
        for (TagButton button : buttons) {
            final var key = button.getKey();
            final var label = new ColoredLabel(key, button.getTag());
            add(label);
            tagButtons.put(key, new TagButtonHolder(button, label));
        }
    }

    public void toggle(String key) {
        final var holder = tagButtons.get(key);
        if (holder != null) {
            holder.getLabel().toggle();
        }
    }

    public Set<String> getAllTags() {
        return filterTags(h -> true);
    }

    public Set<String> getEnabledTags() {
        return filterTags(enabledLabelPredicate());
    }

    public Set<String> getDisabledTags() {
        return filterTags(enabledLabelPredicate().negate());
    }

    private Predicate<TagButtonHolder> enabledLabelPredicate() {
        return h -> h.getLabel().isToggled();
    }

    private Set<String> filterTags(Predicate<TagButtonHolder> predicate) {
        return tagButtons.values().stream()
                .filter(predicate)
                .map(h -> h.getTagButton().getTag())
                .collect(Collectors.toSet());
    }

    public void enableSupportedTags(Set<String> tags) {
        tagButtons.forEach((key, holder) -> {
            final var label = holder.getLabel();
            final var tagButton = holder.getTagButton();
            final var containsTag = tags.contains(tagButton.getTag());
            label.setToggled(containsTag);
        });
    }

    public void changeFontSize(float delta) {
        tagButtons.values().forEach(btn -> UIUtils.changeFontSize(btn.getLabel(), delta));
        UIUtils.changeFontSize(this, delta);
    }
}
