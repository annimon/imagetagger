package com.annimon.imagetagger.views;

import com.annimon.imagetagger.logic.ImageProcessor;
import java.awt.*;

public class ImagePanel extends Component {

    private static final Color TEXT_COLOR = new Color(0x80FFFFFF, true);
    private final ImageProcessor imageProcessor;

    public ImagePanel(ImageProcessor imageProcessor) {
        this.imageProcessor = imageProcessor;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        final var g2d = (Graphics2D) g;
        final int width = getWidth();
        final int height = getHeight();
        g.setColor(getBackground());
        g.fillRect(0, 0, width, height);

        final var resizableImage = imageProcessor.getImage();
        if ((width <= 5) || (height <= 5) || (resizableImage == null)) {
            return;
        }

        var image = resizableImage.getOriginal();
        int imgWidth = resizableImage.getOriginalWidth();
        int imgHeight = resizableImage.getOriginalHeight();
        if (imgWidth > width || imgHeight > height) {
            image = resizableImage.getResized(width, height);
            imgWidth = resizableImage.getResizedWidth();
            imgHeight = resizableImage.getResizedHeight();
        }
        final int x = (width - imgWidth) / 2;
        final int y = (height - imgHeight) / 2;
        g2d.drawImage(image, x, y, this);

        int yy = 5;
        yy = drawString(g2d, imageProcessor.getFilename(), 5, yy);
        yy = drawString(g2d, imageProcessor.getIndexInfo(), 5, yy);
    }

    @SuppressWarnings("SameParameterValue")
    private int drawString(Graphics2D g2d, String filename, int x, int y) {
        final var bounds = g2d.getFontMetrics().getStringBounds(filename, g2d);
        final var strWidth = (int) bounds.getWidth();
        final var strHeight = (int) bounds.getHeight();
        g2d.setColor(TEXT_COLOR);
        g2d.fillRect(x, y, strWidth + 6, strHeight + 6);
        g2d.setColor(Color.BLACK);
        g2d.drawString(filename, x + 2, y + strHeight + 1);
        return y + strHeight + 6;
    }
}
