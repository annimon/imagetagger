package com.annimon.imagetagger.views;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ResizableImage {

    private final Image original;
    private Image resizedImage;
    private int resizedWidth, resizedHeight;

    public ResizableImage(Image original) {
        this.original = original;
    }

    public Image getOriginal() {
        return original;
    }

    public BufferedImage getOriginalAsBufferedImage() {
        return toBufferedImage(original);
    }

    public int getOriginalWidth() {
        return original.getWidth(null);
    }

    public int getOriginalHeight() {
        return original.getHeight(null);
    }

    public Image getResized(int prefWidth, int prefHeight) {
        boolean sizeMatched = (resizedWidth == prefWidth) || (resizedHeight == prefHeight);
        if (resizedImage == null || !sizeMatched) {
            resizedImage = resizeIfNecessary(original, prefWidth, prefHeight);
            resizedWidth = resizedImage.getWidth(null);
            resizedHeight = resizedImage.getHeight(null);
        }
        return resizedImage;
    }

    public BufferedImage getResizedAsBufferedImage(int prefWidth, int prefHeight) {
        return toBufferedImage(getResized(prefWidth, prefHeight));
    }

    public int getResizedWidth() {
        return resizedWidth;
    }

    public int getResizedHeight() {
        return resizedHeight;
    }

    private static Image resizeIfNecessary(Image image, int prefWidth, int prefHeight) {
        final int width = image.getWidth(null);
        final int height = image.getHeight(null);
        if (width <= prefWidth && height <= prefHeight) {
            return image;
        }
        int newWidth = width;
        int newHeight = height;
        if (width > prefWidth) {
            newWidth = prefWidth;
            newHeight = height * newWidth / width;
        }
        if (newHeight > prefHeight) {
            newHeight = prefHeight;
            newWidth = width * newHeight / height;
        }
        return image.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
    }

    private static BufferedImage toBufferedImage(Image image) {
        final int width = image.getWidth(null);
        final int height = image.getHeight(null);
        final var img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final var g = img.createGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return img;
    }
}
