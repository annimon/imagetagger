package com.annimon.imagetagger.logic;

import com.annimon.imagetagger.views.ResizableImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;
import javax.imageio.ImageIO;

public class FilesProvider {

    protected final File dir;

    public FilesProvider(String dir) {
        this.dir = new File(dir);
    }

    public Stream<File> stream() {
        final var files = dir.listFiles();
        if (files == null) {
            throw new RuntimeException("There are no files in directory " + dir);
        }
        return Arrays.stream(files);
    }

    public ResizableImage loadImage(File file) {
        try {
            return new ResizableImage(ImageIO.read(file));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void preloadImage(File file) { }
}
