package com.annimon.imagetagger.logic;

import com.annimon.imagetagger.beans.ImageInfo;
import com.annimon.imagetagger.views.ResizableImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImageProcessor {

    private final FilesProvider files;
    private final String filter;
    private final String sort;
    private final List<ImageInfo> imageInfos;
    private int imagesCount;
    private int index;
    private ImageInfo currentInfo;
    private ResizableImage image;

    public ImageProcessor(FilesProvider files, String filter, String sort) {
        this.files = files;
        this.filter = filter;
        this.sort = sort;
        imageInfos = new ArrayList<>();
        imagesCount = 0;
    }

    public ResizableImage getImage() {
        return image;
    }

    public String getFilename() {
        if (currentInfo != null) {
            return currentInfo.getFile().getName();
        }
        return "";
    }

    public String getIndexInfo() {
        int tagsCount = (currentInfo == null) ? 0 : currentInfo.getTags().size();
        return String.format("%d / %d, %d tags",
                index + 1, imagesCount, tagsCount);
    }

    public void prevImage() {
        index--;
        if (index < 0) {
            index = imagesCount - 1;
        }
        loadImage();
        preloadImage(-1);
    }

    public void nextImage() {
        index++;
        if (index >= imagesCount) {
            index = 0;
        }
        loadImage();
        preloadImage(1);
    }

    public void writeTagsToFile() {
        if (currentInfo != null) {
            currentInfo.writeTagFile();
        }
    }

    public Set<String> getTags() {
        if (currentInfo != null) {
            return currentInfo.getTags();
        }
        return Collections.emptySet();
    }

    public void mergeTags(Set<String> tagsToAdd, Set<String> tagsToRemove) {
        if (currentInfo != null) {
            final var tags = new HashSet<>(currentInfo.getTags());
            tags.removeAll(tagsToRemove);
            tags.addAll(tagsToAdd);
            currentInfo.setTags(tags);
        }
    }

    public void populateFiles() {
        final var infos = files.stream()
                .filter(this::allowedFiles)
                .map(ImageInfo::new)
                .map(ImageInfo::loadTagFile)
                .filter(this::filterTags)
                .collect(Collectors.toList());
        sort(infos);
        imageInfos.clear();
        imageInfos.addAll(infos);
        imagesCount = imageInfos.size();
        if (imagesCount > 0) {
            index = 0;
            loadImage();
            preloadImage(1);
            preloadImage(-1);
        }
    }

    private void loadImage() {
        if (index < 0 || index >= imagesCount) return;

        currentInfo = imageInfos.get(index);
        image = files.loadImage(currentInfo.getFile());
    }

    private void preloadImage(int delta) {
        int newIndex = index + delta;
        if (newIndex < 0) {
            newIndex = imagesCount - 1;
        } else if (newIndex >= imagesCount) {
            newIndex = 0;
        }
        files.preloadImage(imageInfos.get(newIndex).getFile());
    }

    private boolean allowedFiles(File file) {
        final String filename = file.getName().toLowerCase();
        return Stream.of(".jpg", ".png", ".jpeg", ".gif")
                .anyMatch(filename::endsWith);
    }

    private boolean filterTags(ImageInfo info) {
        if (filter.isEmpty()) {
            return true;
        }
        return Arrays.stream(filter.split(","))
                .map(String::trim)
                .filter(s -> s.startsWith("+") || s.startsWith("-"))
                .map(String::toLowerCase)
                .allMatch(s -> {
                    final String tag = s.substring(1);
                    final boolean hasTag = info.getTags().contains(tag);
                    return s.startsWith("-") ? !hasTag : hasTag;
                });
    }

    private void sort(List<ImageInfo> infos) {
        if (sort.contains("random")) {
            Collections.shuffle(infos);
        } else if (sort.contains("name")) {
            infos.sort(Comparator.comparing(ImageInfo::getFile));
        } else if (sort.contains("date")) {
            infos.sort(Comparator.comparingLong(i -> i.getFile().lastModified()));
        } else if (sort.contains("size")) {
            infos.sort(Comparator.comparingLong(i -> i.getFile().length()));
        } else if (sort.contains("tags.count")) {
            infos.sort(Comparator.comparingInt(ImageInfo::getTagsCount));
        }
    }
}
