package com.annimon.imagetagger.logic;

import com.annimon.imagetagger.beans.CacheConfig;
import com.annimon.imagetagger.views.ResizableImage;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CacheableFilesProvider extends FilesProvider {

    private final Executor exe;
    private final LoadingCache<File, ResizableImage> cache;
    private final int maxWidth, maxHeight;

    public CacheableFilesProvider(String dir, CacheConfig cacheConfig) {
        super(dir);
        maxWidth = Math.max(cacheConfig.getWidth(), 100);
        maxHeight = Math.max(cacheConfig.getHeight(), 100);
        exe = Executors.newSingleThreadExecutor();
        cache = Caffeine.newBuilder()
                .executor(exe)
                .maximumSize(Math.max(cacheConfig.getSize(), 1))
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .softValues()
                .build(this::loadAndResize);
    }

    @Override
    public ResizableImage loadImage(File file) {
        final var image = cache.get(file);
        if (image != null) {
            return image;
        } else {
            final var result = loadAndResize(file);
            cache.put(file, result);
            return result;
        }
    }

    @Override
    public void preloadImage(File file) {
        exe.execute(() -> cache.get(file));
    }

    private ResizableImage loadAndResize(File file) {
        final var image = super.loadImage(file);
        return new ResizableImage(image.getResized(maxWidth, maxHeight));
    }
}
