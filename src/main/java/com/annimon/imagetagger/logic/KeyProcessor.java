package com.annimon.imagetagger.logic;

import com.annimon.imagetagger.beans.TagButton;
import com.annimon.imagetagger.views.ImagePanel;
import com.annimon.imagetagger.views.TagPanel;
import com.annimon.imagetagger.views.UIUtils;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class KeyProcessor {

    private static final boolean PREV = false, NEXT = true;

    private final Map<String, List<TagButton>> tags;
    private final Set<String> keys;

    private final List<String> profiles;
    private int profileIndex;

    private TagPanel tagPanel;
    private ImagePanel imagePanel;
    private ImageProcessor imageProcessor;

    public KeyProcessor(Map<String, List<TagButton>> tags, String profile) {
        this.tags = new LinkedHashMap<>(tags);
        this.keys = new HashSet<>();
        this.profiles = new ArrayList<>(tags.keySet());
        this.profileIndex = profiles.indexOf(profile);
        rebuildKeys(tags.get(profile));
    }

    public void setTagPanel(TagPanel tagPanel) {
        this.tagPanel = tagPanel;
    }

    public void setImagePanel(ImagePanel imagePanel) {
        this.imagePanel = imagePanel;
    }

    public void setImageProcessor(ImageProcessor imageProcessor) {
        this.imageProcessor = imageProcessor;
    }

    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                switchImage(PREV);
                break;
            case KeyEvent.VK_RIGHT:
                switchImage(NEXT);
                break;
            case KeyEvent.VK_ENTER:
                imageProcessor.mergeTags(tagPanel.getEnabledTags(), tagPanel.getDisabledTags());
                imageProcessor.writeTagsToFile();
                break;
            case KeyEvent.VK_PAGE_UP:
                switchProfile(PREV);
                break;
            case KeyEvent.VK_PAGE_DOWN:
                switchProfile(NEXT);
                break;
            case KeyEvent.VK_F1:
                imageInfo();
                break;
            case KeyEvent.VK_F3:
                changeBackground();
                break;
            // Ctrl+Plus / Ctrl+Minus
            case KeyEvent.VK_ADD:
            case KeyEvent.VK_EQUALS:
            case KeyEvent.VK_MINUS:
                if ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == 0) break;
                final float delta = (e.getKeyCode() == KeyEvent.VK_MINUS) ? -0.5f : 0.5f;
                UIUtils.changeFontSize(imagePanel, delta);
                tagPanel.changeFontSize(delta);
                break;
            default:
                final var key = getKeyAsString(e);
                if (keys.contains(key)) {
                    tagPanel.toggle(key);
                }
        }
    }

    private void switchImage(boolean switchTo) {
        imageProcessor.mergeTags(tagPanel.getEnabledTags(), tagPanel.getDisabledTags());
        if (switchTo == PREV) {
            imageProcessor.prevImage();
        } else {
            imageProcessor.nextImage();
        }
        imagePanel.repaint();
        tagPanel.enableSupportedTags(imageProcessor.getTags());
        tagPanel.repaint();
    }

    private void switchProfile(boolean switchTo) {
        profileIndex += (switchTo == PREV) ? -1 : 1;
        if (profileIndex < 0) profileIndex = profiles.size() - 1;
        else if (profileIndex >= profiles.size()) profileIndex = 0;
        final String profile = profiles.get(profileIndex);
        rebuildKeys(tags.get(profile));
        tagPanel.rebuildTags(tags.get(profile));
        tagPanel.enableSupportedTags(imageProcessor.getTags());
        tagPanel.repaint();
    }

    private void imageInfo() {
        System.out.println(imageProcessor.getFilename());
        final var image = imageProcessor.getImage();
        if (image != null) {
            System.out.println(image.getOriginalWidth() + "x" + image.getOriginalHeight());
        }
        System.out.println(imageProcessor.getTags()
                .stream()
                .sorted()
                .collect(Collectors.joining(", ")));
        System.out.println();
    }

    private void changeBackground() {
        // It's not necessary to convert color to gray
        // Just use one of it's color components
        int r = imagePanel.getBackground().getRed();
        int gray = r;
        final int[] colors = {0, 40, 64, 128, 192, 236, 255};
        for (int i = 0; i < colors.length; i++) {
            if (r >= colors[i]) {
                int index = i + 1;
                if (index >= colors.length) {
                    index = 0;
                }
                gray = colors[index];
            }
        }
        final var bg = new Color(gray, gray, gray);
        imagePanel.setBackground(bg);
        imagePanel.repaint();
        tagPanel.setBackground(bg);
        tagPanel.repaint();
    }

    private void rebuildKeys(List<TagButton> tags) {
        keys.clear();
        keys.addAll(tags.stream()
                .map(TagButton::getKey)
                .collect(Collectors.toSet()));
    }

    private String getKeyAsString(KeyEvent e) {
        String prefix = "";
        if ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
            prefix += "Ctrl+";
        }
        if ((e.getModifiersEx() & KeyEvent.ALT_DOWN_MASK) != 0) {
            prefix += "Alt+";
        }
        return prefix + keyToChar(e);
    }

    private String keyToChar(KeyEvent e) {
        final var code = e.getKeyCode();
        if (code >= KeyEvent.VK_A && code <= KeyEvent.VK_Z) {
            String key = String.valueOf((char) ('a' + (code - KeyEvent.VK_A)));
            if ((e.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) != 0) {
                key = key.toUpperCase(Locale.ROOT);
            }
            return key;
        }
        return String.valueOf(e.getKeyChar());
    }
}
