package com.annimon.imagetagger;

import com.annimon.imagetagger.beans.Config;
import com.annimon.imagetagger.logic.CacheableFilesProvider;
import com.annimon.imagetagger.logic.ImageProcessor;
import com.annimon.imagetagger.logic.KeyProcessor;
import com.annimon.imagetagger.views.ImagePanel;
import com.annimon.imagetagger.views.TagPanel;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.*;

public class Main extends JFrame {

    public static void main(String[] args) throws IOException {
        final var file = new File("imagetagger.json");
        final var mapper = new ObjectMapper();
        final var config = mapper.readValue(file, Config.class);
        if (args.length > 0 && args[0].equalsIgnoreCase("validate")) {
            config.validate();
            return;
        }

        EventQueue.invokeLater(() -> {
            var main = new Main(config);
            main.setVisible(true);
        });
    }

    public Main(Config config) {
        super("ImageTagger");

        final var tagButtons = config.getButtons(config.getProfile());
        final var tagPanel = new TagPanel(tagButtons);
        final var files = new CacheableFilesProvider(config.getDir(), config.getCache());
        final var imageProcessor = new ImageProcessor(files, config.getFilter(), config.getSort());
        final var imagePanel = new ImagePanel(imageProcessor);
        final var keyProcessor = new KeyProcessor(config.getTags(), config.getProfile());
        keyProcessor.setTagPanel(tagPanel);
        keyProcessor.setImagePanel(imagePanel);
        keyProcessor.setImageProcessor(imageProcessor);
        imageProcessor.populateFiles();
        tagPanel.enableSupportedTags(imageProcessor.getTags());

        final Color background = new Color(40, 40, 40);
        tagPanel.setBackground(background);
        imagePanel.setBackground(background);

        setPreferredSize(new Dimension(800, 600));
        setLayout(new BorderLayout());
        add(tagPanel, BorderLayout.SOUTH);
        add(imagePanel, BorderLayout.CENTER);
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                keyProcessor.keyPressed(e);
            }
        });
        setResizable(true);
        setLocationByPlatform(true);
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
    }
}
