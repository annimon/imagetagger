package com.annimon.imagetagger.beans;

public class TagButton {

    private String key;
    private String tag;

    public TagButton() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
