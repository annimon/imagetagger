package com.annimon.imagetagger.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CacheConfig {

    public static final CacheConfig DEFAULT = new CacheConfig(20, 1920, 1080);

    private Integer size;
    private Integer width;
    private Integer height;

    public CacheConfig() {
    }

    public CacheConfig(Integer size, Integer width, Integer height) {
        this.size = size;
        this.width = width;
        this.height = height;
    }

    public int getSize() {
        return Objects.requireNonNullElse(size, DEFAULT.size);
    }

    public int getWidth() {
        return Objects.requireNonNullElse(width, DEFAULT.width);
    }

    public int getHeight() {
        return Objects.requireNonNullElse(height, DEFAULT.height);
    }
}
