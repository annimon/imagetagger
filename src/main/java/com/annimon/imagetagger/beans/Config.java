package com.annimon.imagetagger.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Config {

    private String dir;
    private CacheConfig cache;
    private String filter;
    private String sort;
    private String profile;
    private Map<String, List<TagButton>> tags;

    public Config() {
    }

    public String getDir() {
        return dir;
    }

    public CacheConfig getCache() {
        return Objects.requireNonNullElse(cache, CacheConfig.DEFAULT);
    }

    public String getFilter() {
        return Objects.requireNonNullElse(filter, "");
    }

    public String getSort() {
        return Objects.requireNonNullElse(sort, "");
    }

    public String getProfile() {
        return profile;
    }

    public Map<String, List<TagButton>> getTags() {
        return tags;
    }

    public List<TagButton> getButtons(String profile) {
        return tags.get(profile);
    }

    public void validate() {
        final var errors = new HashSet<String>();
        final var uniqueTags = new HashMap<String, String>();
        final var uniqueKeys = new HashSet<String>();
        for (var entry : tags.entrySet()) {
            final var profile = entry.getKey();
            final var tagButtons = entry.getValue();

            uniqueKeys.clear();
            for (var tb : tagButtons) {
                // Detect duplicate keys in profile
                boolean isUnique = uniqueKeys.add(tb.getKey());
                if (!isUnique) {
                    errors.add(String.format("Duplicate key %s in profile %s", tb.getKey(), profile));
                }

                // Detect duplicate tags globally
                String previousProfile = uniqueTags.put(tb.getTag(), profile);
                if (previousProfile != null) {
                    errors.add(String.format("Tag %s in profile %s was already defined in %s", tb.getTag(), profile, previousProfile));
                }
            }
        }
        if (errors.isEmpty()) {
            System.out.println("All checks passed");
        } else {
            errors.forEach(System.err::println);
        }
    }
}
