package com.annimon.imagetagger.beans;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ImageInfo {
    private static final String PREFIX = "-IPTC:keywords=";

    private final File file;
    private final Set<String> tags;

    public ImageInfo(File file) {
        this.file = file;
        tags = new HashSet<>();
    }

    public File getFile() {
        return file;
    }

    public int getTagsCount() {
        return tags.size();
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Collection<String> newTags) {
        tags.clear();
        tags.addAll(newTags);
    }

    public ImageInfo loadTagFile() {
        final var tagFilePath = tagFilePath();
        if (Files.exists(tagFilePath)) {
            try {
                final var lines = Files.lines(tagFilePath)
                        .map(s -> s.replace(PREFIX, ""))
                        .collect(Collectors.toList());
                setTags(lines);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return this;
    }

    public void writeTagFile() {
        final var lines = tags.stream()
                .sorted()
                .map(s -> PREFIX + s)
                .collect(Collectors.toList());
        try {
            Files.write(tagFilePath(), lines);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Path tagFilePath() {
        return Path.of(file.getParent(), file.getName() + ".txt");
    }
}
