package com.annimon.imagetagger.beans;

import com.annimon.imagetagger.views.ColoredLabel;

public class TagButtonHolder {

    private final TagButton tagButton;
    private final ColoredLabel label;

    public TagButtonHolder(TagButton tagButton, ColoredLabel label) {
        this.tagButton = tagButton;
        this.label = label;
    }

    public TagButton getTagButton() {
        return tagButton;
    }

    public ColoredLabel getLabel() {
        return label;
    }
}
