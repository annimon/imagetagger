# Image Tagger

This application allows adding tags to the images by pressing configured keys.

## Configuration

In the configuration file `imagetagger.json` you can declare a directory for images, set cache entries limit and resolution, add filters and configure keys and tags.

## Shortcuts

- `Left/Right` — previous/next image
- `Enter` — creates/updates .txt file with tags
- `F3` — change background color

## Filtration

To filter input images you can set a filter.

For example: `"filter": "+morning"` will show files containing `morning` tag, `"filter": "-morning"` — files not containing `morning` tag

## Actual applying tag to image

By using [ExifTool](https://exiftool.org/) and simple script you can write tags to image:

```bash
# Bash
for i in *.jpg; do exiftool -q -overwrite_original -@ "$i.txt" "$i"; done
```

```cmd
rem CMD
for /f "delims=" %f in ('dir /b "*.jpg"') do exiftool -q -overwrite_original -@ "%~nxf.txt" "%f"
```

The program requires Java 11 to run.


## Changelog

**1.2**
 - Ability to change font size with `Ctrl+Plus` and `Ctrl+Minus`
 - Ability to change profiles with `Page Up` and `Page Down`
 - Dark background and maximized window by default

**1.1**
 - Added an ability to set modifiers Ctrl/Alt:  `Ctrl+Alt+s`, `Ctrl+Alt+S`.
 - `F3` for changing background color
